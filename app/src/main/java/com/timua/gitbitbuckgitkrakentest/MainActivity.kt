package com.timua.gitbitbuckgitkrakentest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun selectBtnClicked(view: View) {
        showTextTxt.text = "Hello, how are you?"
        showTextTxt.text = ""
        showTextTxt.text = 2.toString()

        showTextTxt.text = "Create new branch"
    }
}
